# Container for Natbraille-editor + Natbraille-editor-server 

This image builds a container for local usage from sources

It embeds libreoffice and unoconverter.

User can login with any login/password (no user db, no check)

## License

This is free software, distributed under the terms of the GNU GENERAL PUBLIC LICENSE Version 3. 
For more information, see the file COPYING. 

## build & run

Build with

        sudo docker build -t natbraille-editor-server-bundle .

Run using

        sudo docker run -d -p 4567:4567 natbraille-editor-server-bundle

To execute, point your browser to

        http://localhost:4567/
