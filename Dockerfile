FROM debian:bookworm

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \    
    git \
    openjdk-17-jdk-headless \
    unzip \
    curl \
    gettext \
    xz-utils


RUN mkdir -p /opt/bin
RUN mkdir -p /datas/src

#
# BACKEND
#

# install gradle

WORKDIR /opt/bin
RUN curl https://downloads.gradle.org/distributions/gradle-7.6.2-bin.zip --output gradle.zip && \
    unzip gradle.zip && \
    rm gradle.zip && \
    /opt/bin/gradle-7.6.2/bin/gradle --version

# clone & compile backend

WORKDIR /datas/src
RUN git clone -b dev https://framagit.org/natbraille-association/natbraille.git
WORKDIR /datas/src/natbraille
RUN /opt/bin/gradle-7.6.2/bin/gradle -p natbraille-editor-server shadowJar

#
# FRONTEND
# 

# install node

WORKDIR /opt/bin
RUN curl https://nodejs.org/dist/v20.4.0/node-v20.4.0-linux-x64.tar.xz --output node.xz && \
    xz -dc node.xz | tar x && \
    ls -al . && \
    /opt/bin/node-v20.4.0-linux-x64/bin/node --version
ENV PATH="$PATH:/opt/bin/node-v20.4.0-linux-x64/bin/" 


# clone & compile frontend

WORKDIR /datas/src
RUN git clone -b main https://framagit.org/natbraille-association/natbraille-editor.git
WORKDIR /datas/src/natbraille-editor
RUN npm i && \
    npm run build


#
# POSTGRES
#

# RUN apt-get install -y postgresql
# 
# USER postgres
# RUN psql -U postgres -a -f /datas/src/natbraille/natbraille-editor-server/docker-postgres/initdb.d/initdb.sql 
# RUN psql -U postgres -a -c "ALTER USER postgres WITH password 'pgpwd';"


#
# LIBREOFFICE and UNOCONVERTER
#

# libreoffice and unoconverter
RUN apt-get install -y \
    libreoffice \
    pip
 
# non --break-system-packages requires setting a python env    
RUN pip install unoserver --break-system-packages
RUN 

#
# APPLICATION
#

RUN mkdir -p /datas/server
WORKDIR /datas/server

# setup conf
RUN echo '{"host": "0.0.0.0","port":"4567","publicServerURLForExternalLinks" : "http://127.0.0.1:4567","useUniversalApiToken": true,"documentsRoot": "./SERVER_DOCUMENTS_ROOT","useDb" : false,"staticFilesDirectory" : "/datas/src/natbraille-editor/dist/","devURL" : ""}' > server.conf.json

# setup startup script
RUN echo 'unoserver & java -jar /datas/src/natbraille/natbraille-editor-server/build/libs/natbraille-editor-server-1.0-SNAPSHOT-all.jar' > start-server.sh

EXPOSE 4567
CMD sh start-server.sh
